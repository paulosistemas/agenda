package com.Itau;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.List;

public class Agenda {
    private List<Pessoa> agenda;
    private int idLast=-1;

    public Agenda() {
        agenda = new ArrayList<Pessoa>();
        adicionarContato();
        controlarMenu();
    }
    public void controlarMenu(){
        Scanner inputOpcao = new Scanner(System.in);
        for(int opcao = 0; opcao != 9; ){
            System.out.println("Digite 9 -> sair, 1 -> novo contato, 2 -> consultar tudo , 3 -> consultar pelo id , 4 -> consultar pelo nome");
            System.out.println("5 -> Remover Contato (ID), 6 -> Remover Contato (Nome)");
            opcao = inputOpcao.nextInt();

            if (opcao == 1){
                this.adicionarContato();
            }
            if (opcao == 2){
                this.consultarAgenda();
            }
            if (opcao == 3){
                System.out.println("Informe o ID:");
                Scanner id = new Scanner(System.in);
                this.consultarAgendaID(id.nextInt());
            }
            if (opcao == 4) {
                System.out.println("Informe o nome do contato a ser consultato:");
                Scanner id = new Scanner(System.in);
                this.consultarNome(id.nextLine());
            }
            if (opcao == 5) {
                System.out.println("Informe o id do contato a ser excluido:");
                Scanner id = new Scanner(System.in);
                this.removerContato(id.nextInt());
            }
            if (opcao == 6) {
                System.out.println("Informe o nome do contato a ser excluido:");
                Scanner nome = new Scanner(System.in);
                this.removerContato(nome.nextLine());
            }
            if (opcao == 0){
                this.carregarDefault();
            }
        }
        System.out.println("*********** Agenda finalizada com sucesso *********** ");
    }
    public void carregarDefault(){
        adicionarContato("Capitão América", "(11) 99999-1234");
        adicionarContato("Homem de Ferro", "(11) 99999-5678");
        adicionarContato("Mulher Maravilha", "(11) 1234-5678");
    }
    public void adicionarContato(){
        System.out.println("************ Agenda v1.0 ************");
        System.out.println("Informe o nome e telefone da pessoa:");
        Scanner inputNome = new Scanner(System.in);
        Scanner inputTelefone = new Scanner(System.in);
        Pessoa pessoa = new Pessoa(setID(), inputNome.nextLine(), inputTelefone.nextLine());
        agenda.add(pessoa);
        System.out.println("Adicionado com sucesso: " + pessoa.getNome() + " " + pessoa.getTelefone());
    }
    public void adicionarContato(String nome, String telefone){
        Pessoa pessoa = new Pessoa(setID(), nome, telefone);
        agenda.add(pessoa);
        System.out.println("Adicionado com sucesso: " + pessoa.getNome() + " " + pessoa.getTelefone());
    }
    public int setID(){
        return idLast+=1;
    }
    public void consultarAgenda(){
        for (int i=0; i<agenda.size(); i++) {
            System.out.println("Contato ID:" + i + " Nome: " + agenda.get(i).getNome() + " Tel: " + agenda.get(i).getTelefone());
        }
    }
    public void consultarAgendaID(int id){
        System.out.println("Contato ID:" + agenda.get(id).getId() + " Nome: " + agenda.get(id).getNome() + " Tel: " + agenda.get(id).getTelefone());
    }
    public int consultarNome(String nome){
//        System.out.println("nome recebido:" + nome);
        int idContato=0;
        for (int i=0;i<agenda.size();i++){
//            System.out.println("Dentro do for nomes: '" + agenda.get(i).getNome() +"'");
            if (nome.equalsIgnoreCase(agenda.get(i).getNome())){
                System.out.println("ID do contato: " + i);
                idContato=i;
                i=agenda.size();
            }
        }
        return idContato;
    }
    public void removerContato(int id){
        agenda.remove(id);
        System.out.println("***** Removido com sucesso ID:" + id);
    }
    public void removerContato(String nome){
        removerContato(this.consultarNome(nome));
        System.out.println("***** Removido com sucesso Nome:" + nome);
    }
}
